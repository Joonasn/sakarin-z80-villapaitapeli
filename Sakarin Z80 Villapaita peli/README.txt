Sakarin Z80 Villapaita peli (Sakari's Z80 Sweater game)
2008 Original Flash game - Nutshell (from Aasipelit)
2019 TI-83 Z80 Demake - Sopuisasopuli


Tested with a real TI-84+ calculator and WabbitEmu
----------------------------------------------------------------

HOW TO INSTALL:

1.Get MirageOS and install it to your calculator
   Download: https://www.ticalc.org/archives/files/fileinfo/139/13949.html

2. Install Sakari's game to your calculator

3. Have fun for 5 seconds

?. For Emulation, use Wabbitemu: http://wabbitemu.org/ (Works on Windows, Mac and Android!)
------------------------------------------------------------------------------

HOW TO PLAY:

UP/DOWN - Control on menus
ENTER - Select
Clear - End game
--------------------------------------------------------------------------------

FOR DEVELOPMENT PURPOSES:

1. For now, "MirageOS Development Include File" is used in the source code
   Download: https://www.ticalc.org/archives/files/fileinfo/139/13949.html

2. Images were created in MS Paint and "BMPZ80 Graphics Utility for Windows"
   Download: https://www.ticalc.org/pub/win/graphics/bmpz80.zip

3. Compiled by using "Spasm"
   Download: https://github.com/alberthdev/spasm-ng
   






